module main

go 1.15

require (
	gitlab.com/lucaskoontz/another-go-demo-module v0.1.0
	gitlab.com/lucaskoontz/renovate/go-demo-module v0.1.0
)
