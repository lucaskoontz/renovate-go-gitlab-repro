package main

import (
	anotherDemo "gitlab.com/lucaskoontz/another-go-demo-module"
	"gitlab.com/lucaskoontz/renovate/go-demo-module"
)

func main() {
	demo.Echo("Renovate")
	anotherDemo.Echo("Another Renovate")
}
